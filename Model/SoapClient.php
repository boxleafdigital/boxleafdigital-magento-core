<?php declare(strict_types=1);

namespace BoxLeafDigital\Core\Model;

use BoxLeafDigital\Core\Api\Data\RestLogInterfaceFactory;
use BoxLeafDigital\Core\Api\RestLogRepositoryInterface;
use BoxLeafDigital\Core\Api\RestLogRepositoryInterfaceFactory;
use BoxLeafDigital\Core\Model\Http\Client\CurlFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Profiler;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Webapi\Soap\ClientFactory;

/**
 * Class SoapClient
 * @author Daniel Coull <hello@boxleafdigital.com>
 * @package BoxLeafDigital\Core\Model\RestFramework
 */
class SoapClient
{

    /**
     * @var CurlFactory
     */
    private $_curl;

    /**
     * @var RestLogRepositoryInterface
     */
    private $_restLogRepository;

    /**
     * @var RestLogInterfaceFactory
     */
    private $_restLog;

    /**
     * @var Json
     */
    private $jsonSerializer;

    /**
     * @var $_url
     */
    private $_url;

    /**
     * @var $_type
     */
    private $_type;

    private $_authType = 'user';
    private $_authArray = [];

    const PROFILER_REQUEST = 'BoxLeafDigital_Core_API::Request';
    /**
     * @var ClientFactory
     */
    private $soapClient;

    public function __construct(
        ClientFactory $soapClient,
        RestLogRepositoryInterfaceFactory $restLogRepository,
        RestLogInterfaceFactory $restLog,
        Json $jsonSerializer
    ) {
        $this->soapClient = $soapClient;
        $this->_restLogRepository = $restLogRepository;
        $this->_restLog = $restLog;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setWsdl($url)
    {
        $this->_url = $url;
        return $this;
    }

    /**
     * @return string
     */
    protected function getWsdl(): string
    {
        return $this->_url;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }

    /**
     * @return string
     */
    protected function getType(): string
    {
        return $this->_type;
    }

    /**
     * @param $processTime
     * @param $client
     * @param $function
     * @param string $responseMessage
     * @return bool
     */
    protected function logRest($processTime, $client, $function, $responseMessage = ''): bool
    {
        $restLog = $this->_restLog->create();
        $restLog->setUrlPath($this->getWsdl());

        $restLog->setRequestParams($function);

        $restLog->setHeaders($this->jsonSerializer->serialize($client->__getLastRequestHeaders()));
        $restLog->setResponse($this->jsonSerializer->serialize($client->__getLastResponse()));
        $restLog->setProcessTime($processTime);
        $restLog->setResponseMessage($responseMessage);

        try {
            $this->_restLogRepository->create()->save($restLog);
        } catch (LocalizedException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @param $type
     * @return $this
     */
    public function setAuth($data, $type)
    {
        $this->_authType = $type;
        $this->_authArray = $data;
        return $this;
    }

    /**
     * @param $message
     * @param $function
     * @param array $params
     * @param bool $requiresAuth
     * @param bool $logQuery
     * @return array|bool
     * @throws \SoapFault
     */
    public function runSoapRequest($message, $function, $params = [], $requiresAuth = true, $logQuery = true)
    {
        $response = '';

        Profiler::start(self::PROFILER_REQUEST);
        Profiler::start($message);

        $requestStartTime = microtime(true);

        $request =  $this->soapClient->create($this->getWsdl(), ['trace'=> 1,'keep_alive' => false,
            'connection_timeout' => 50,
            'cache_wsdl' => WSDL_CACHE_BOTH,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE
        ]);

        try {
            $response = $request->$function(...$params);
        } catch (\Exception $e) {
            if ($e->getMessage() == 'Could not connect to host') {
                sleep(10);
                $this->runSoapRequest($message, $function, $params, $requiresAuth, $logQuery);
            } else {
                var_dump($e->getMessage());
                return false;
            }
        }

        $requestEndTime = microtime(true) - $requestStartTime;

        Profiler::stop($message);
        Profiler::stop(self::PROFILER_REQUEST);

        if ($logQuery == true) {
            $this->logRest($requestEndTime, $request, $function, $message);
        }

        return $response;
    }
}
