<?php declare(strict_types=1);

namespace BoxLeafDigital\Core\Model;

use BoxLeafDigital\Core\Api\Data\RestLogInterface;
use BoxLeafDigital\Core\Api\Data\RestLogInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * Class RestLog
 * @package BoxLeafDigital\Core\Model
 */
class RestLog extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'boxleafdigital_core_restlog';
    protected $restlogDataFactory;

    protected $dataObjectHelper;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param RestLogInterfaceFactory $restlogDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \BoxLeafDigital\Core\Model\ResourceModel\RestLog $resource
     * @param \BoxLeafDigital\Core\Model\ResourceModel\RestLog\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        RestLogInterfaceFactory $restlogDataFactory,
        DataObjectHelper $dataObjectHelper,
        \BoxLeafDigital\Core\Model\ResourceModel\RestLog $resource,
        \BoxLeafDigital\Core\Model\ResourceModel\RestLog\Collection $resourceCollection,
        array $data = []
    ) {
        $this->restlogDataFactory = $restlogDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve restlog model with restlog data
     * @return RestLogInterface
     */
    public function getDataModel()
    {
        $restlogData = $this->getData();

        $restlogDataObject = $this->restlogDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $restlogDataObject,
            $restlogData,
            RestLogInterface::class
        );

        return $restlogDataObject;
    }
}
