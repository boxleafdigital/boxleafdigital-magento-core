<?php declare(strict_types=1);

namespace BoxLeafDigital\Core\Model;

use BoxLeafDigital\Core\Api\Data\RestLogInterfaceFactory;
use BoxLeafDigital\Core\Api\RestLogRepositoryInterface;
use BoxLeafDigital\Core\Api\RestLogRepositoryInterfaceFactory;
use BoxLeafDigital\Core\Model\Http\Client\CurlFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Profiler;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class RestClient
 * @author Daniel Coull <hello@boxleafdigital.com>
 * @package BoxLeafDigital\Core\Model\RestFramework
 */
class RestClient
{
    /**
     * @var CurlFactory
     */
    private $_curl;

    /**
     * @var RestLogRepositoryInterface
     */
    private $_restLogRepository;

    /**
     * @var RestLogInterfaceFactory
     */
    private $_restLog;

    /**
     * @var Json
     */
    private $jsonSerializer;

    /**
     * @var $_headers
     */
    private $_headers;

    /**
     * @var $_params
     */
    private $_params;

    /**
     * @var $_url
     */
    private $_url;

    /**
     * @var $_type
     */
    private $_type;

    private $_authType = 'user';
    private $_authArray = [];

    const PROFILER_REQUEST = 'BoxLeafDigital_Core_API::Request';

    public function __construct(
        CurlFactory $curl,
        RestLogRepositoryInterfaceFactory $restLogRepository,
        RestLogInterfaceFactory $restLog,
        Json $jsonSerializer
    ) {
        $this->_curl = $curl;
        $this->_restLogRepository = $restLogRepository;
        $this->_restLog = $restLog;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @param $headers
     * @return $this
     */
    public function setHeaders($headers)
    {
        $this->_headers = $headers;
        return $this;
    }

    /**
     * @return array
     */
    protected function getHeaders(): array
    {
        return $this->_headers;
    }

    /**
     * @param $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * @return array
     */
    protected function getParams(): array
    {
        if ($this->_params == null) {
            return [];
        }
        return $this->_params;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }

    /**
     * @return string
     */
    protected function getUrl(): string
    {
        return $this->_url;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }

    /**
     * @return string
     */
    protected function getType(): string
    {
        return $this->_type;
    }

    public function queryToArray($qry)
    {
        $result = [];
        //string must contain at least one = and cannot be in first position
        if (strpos($qry, '=')) {
            if (strpos($qry, '?')!==false) {
                $q = parse_url($qry);
                $qry = $q['query'];
            }
        } else {
            return false;
        }

        foreach (explode('&', $qry) as $couple) {
            list($key, $val) = explode('=', $couple);
            $result[$key] = $val;
        }

        return empty($result) ? false : $result;
    }
    /**
     * @param $processTime
     * @param $response
     * @param string $responseMessage
     * @return bool
     */
    protected function logRest($processTime, $response, $responseMessage = ''): bool
    {
        $restLog = $this->_restLog->create();
        $restLog->setUrlPath($this->getUrl());

        if ($this->getType() == 'GET') {
            $params = $this->queryToArray($this->getUrl());
            $restLog->setRequestParams($this->jsonSerializer->serialize($params));
        } else {
            $restLog->setRequestParams($this->jsonSerializer->serialize($this->getParams()));
        }

        $restLog->setHeaders($this->jsonSerializer->serialize($this->getHeaders()));
        $restLog->setResponse($this->jsonSerializer->serialize($response));
        $restLog->setProcessTime($processTime);
        $restLog->setResponseMessage($responseMessage);

        try {
            $this->_restLogRepository->create()->save($restLog);
        } catch (LocalizedException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @param $type
     * @return $this
     */
    public function setAuth($data, $type)
    {
        $this->_authType = $type;
        $this->_authArray = $data;
        return $this;
    }

    /**
     * @param $message
     * @param bool $requiresAuth
     * @param bool $logQuery
     * @return array
     */
    public function runRest($message, $requiresAuth = true, $logQuery = true): array
    {
        Profiler::start(self::PROFILER_REQUEST);
        Profiler::start($message);

        $requestStartTime = microtime(true);

        $curl = $this->_curl->create();
        $curl->setHeaders($this->getHeaders());
        $curl->setTimeout(1800);

        if ($requiresAuth == true) {
            switch (strtolower($this->_authType)) {
                case 'cookie':
                    $curl->setCookies($this->_authArray);
                    break;
                default:
                case 'user':
                    $curl->setCredentials($this->_authArray['user'], $this->_authArray['pass']);
                    break;
            }
        }

        switch (strtolower($this->_type)) {
            default:
            case 'post':
                $curl->post($this->_url, json_encode($this->_params));
                break;
            case 'get':
                $curl->get($this->_url);
                break;
            case 'patch':
                $curl->patch($this->_url, json_encode($this->_params));
                break;
            case 'delete':
                $curl->delete($this->_url, $this->_params);
                break;
             case 'put':
                $curl->put($this->_url, $this->_params);
                break;
        }

        try {
            $response = $this->jsonSerializer->unserialize($curl->getBody());
        } catch (\Exception $e) {
            $response = ['error' => true, 'code'=> $e->getCode(), 'message' => $e->getMessage()];
            var_dump($curl->getBody());
        }

        $requestEndTime = microtime(true) - $requestStartTime;

        Profiler::stop($message);
        Profiler::stop(self::PROFILER_REQUEST);

        if ($logQuery == true) {
            $this->logRest($requestEndTime, $response, $message);
        }

        return $response;
    }
}
