<?php declare(strict_types=1);

namespace BoxLeafDigital\Core\Model\Http\Client;

/**
 * Class Curl
 * @package BoxLeafDigital\Core\Model\Http\Client
 * @author Daniel Coull <hello@boxleafdigital.com>
 */
class Curl extends \Magento\Framework\HTTP\Client\Curl
{
    /**
     * @param $uri
     * @param $params
     */
    public function delete($uri, $params)
    {
        $this->makeRequest("DELETE", $uri, $params);
    }

    /**
     * @param $uri
     * @param $params
     */
    public function patch($uri, $params)
    {
        $this->makeRequest("PATCH", $uri, $params);
    }

    /**
     * @param $uri
     * @param $params
     */
    public function put($uri, $params)
    {
        $this->makeRequest("PUT", $uri, $params);
    }
}
