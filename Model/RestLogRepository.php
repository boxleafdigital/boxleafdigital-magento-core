<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Model;

use BoxLeafDigital\Core\Api\Data\RestLogInterfaceFactory;
use BoxLeafDigital\Core\Api\Data\RestLogSearchResultsInterfaceFactory;
use BoxLeafDigital\Core\Api\RestLogRepositoryInterface;
use BoxLeafDigital\Core\Model\ResourceModel\RestLog as ResourceRestLog;
use BoxLeafDigital\Core\Model\ResourceModel\RestLog\CollectionFactory as RestLogCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class RestLogRepository
 * @package BoxLeafDigital\Core\Model
 */
class RestLogRepository implements RestLogRepositoryInterface
{

    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var RestLogSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var RestLogFactory
     */
    protected $restLogFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var RestLogInterfaceFactory
     */
    protected $dataRestLogFactory;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;
    /**
     * @var ResourceRestLog
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var RestLogCollectionFactory
     */
    protected $restLogCollectionFactory;


    /**
     * @param ResourceRestLog $resource
     * @param RestLogFactory $restLogFactory
     * @param RestLogInterfaceFactory $dataRestLogFactory
     * @param RestLogCollectionFactory $restLogCollectionFactory
     * @param RestLogSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceRestLog $resource,
        RestLogFactory $restLogFactory,
        RestLogInterfaceFactory $dataRestLogFactory,
        RestLogCollectionFactory $restLogCollectionFactory,
        RestLogSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->restLogFactory = $restLogFactory;
        $this->restLogCollectionFactory = $restLogCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRestLogFactory = $dataRestLogFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \BoxLeafDigital\Core\Api\Data\RestLogInterface $restLog
    ) {
        /* if (empty($restLog->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $restLog->setStoreId($storeId);
        } */

        $restLogData = $this->extensibleDataObjectConverter->toNestedArray(
            $restLog,
            [],
            \BoxLeafDigital\Core\Api\Data\RestLogInterface::class
        );

        $restLogModel = $this->restLogFactory->create()->setData($restLogData);

        try {
            $this->resource->save($restLogModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the restLog: %1',
                $exception->getMessage()
            ));
        }
        return $restLogModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($restLogId)
    {
        $restLog = $this->restLogFactory->create();
        $this->resource->load($restLog, $restLogId);
        if (!$restLog->getId()) {
            throw new NoSuchEntityException(__('RestLog with id "%1" does not exist.', $restLogId));
        }
        return $restLog->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->restLogCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \BoxLeafDigital\Core\Api\Data\RestLogInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \BoxLeafDigital\Core\Api\Data\RestLogInterface $restLog
    ) {
        try {
            $restLogModel = $this->restLogFactory->create();
            $this->resource->load($restLogModel, $restLog->getRestlogId());
            $this->resource->delete($restLogModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the RestLog: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($restLogId)
    {
        return $this->delete($this->get($restLogId));
    }
}

