<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Model\ResourceModel;

/**
 * Class RestLog
 * @package BoxLeafDigital\Core\Model\ResourceModel
 * @author  Daniel Coull <hello@boxleafdigital.com>
 */
class RestLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('boxleafdigital_core_restlog', 'restlog_id');
    }
}

