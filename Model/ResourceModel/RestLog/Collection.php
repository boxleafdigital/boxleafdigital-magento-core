<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Model\ResourceModel\RestLog;

/**
 * Class Collection
 * @package BoxLeafDigital\Core\Model\RestLog
 * @author  Daniel Coull <hello@boxleafdigital.com>
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'restlog_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \BoxLeafDigital\Core\Model\RestLog::class,
            \BoxLeafDigital\Core\Model\ResourceModel\RestLog::class
        );
    }
}

