<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Model\Data;

use BoxLeafDigital\Core\Api\Data\RestLogInterface;

/**
 * Class RestLog
 * @package BoxLeafDigital\Core\Model\Data
 * @author  Daniel Coull <hello@boxleafdigital.com>
 */
class RestLog extends \Magento\Framework\Api\AbstractExtensibleObject implements RestLogInterface
{

    /**
     * Get restlog_id
     * @return string|null
     */
    public function getRestlogId()
    {
        return $this->_get(self::RESTLOG_ID);
    }

    /**
     * Set restlog_id
     * @param string $restlogId
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setRestlogId($restlogId)
    {
        return $this->setData(self::RESTLOG_ID, $restlogId);
    }

    /**
     * Get url_path
     * @return string|null
     */
    public function getUrlPath()
    {
        return $this->_get(self::URL_PATH);
    }

    /**
     * Set url_path
     * @param string $urlPath
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setUrlPath($urlPath)
    {
        return $this->setData(self::URL_PATH, $urlPath);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\Core\Api\Data\RestLogExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\Core\Api\Data\RestLogExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\Core\Api\Data\RestLogExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get request_params
     * @return string|null
     */
    public function getRequestParams()
    {
        return $this->_get(self::REQUEST_PARAMS);
    }

    /**
     * Set request_params
     * @param string $requestParams
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setRequestParams($requestParams)
    {
        return $this->setData(self::REQUEST_PARAMS, $requestParams);
    }

    /**
     * Get headers
     * @return string|null
     */
    public function getHeaders()
    {
        return $this->_get(self::HEADERS);
    }

    /**
     * Set headers
     * @param string $headers
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setHeaders($headers)
    {
        return $this->setData(self::HEADERS, $headers);
    }

    /**
     * Get response
     * @return string|null
     */
    public function getResponse()
    {
        return $this->_get(self::RESPONSE);
    }

    /**
     * Set response
     * @param string $response
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setResponse($response)
    {
        return $this->setData(self::RESPONSE, $response);
    }

    /**
     * Get response_message
     * @return string|null
     */
    public function getResponseMessage()
    {
        return $this->_get(self::RESPONSE_MESSAGE);
    }

    /**
     * Set response_message
     * @param string $responseMessage
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setResponseMessage($responseMessage)
    {
        return $this->setData(self::RESPONSE_MESSAGE, $responseMessage);
    }

    /**
     * Get process_time
     * @return string|null
     */
    public function getProcessTime()
    {
        return $this->_get(self::PROCESS_TIME);
    }

    /**
     * Set process_time
     * @param string $processTime
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setProcessTime($processTime)
    {
        return $this->setData(self::PROCESS_TIME, $processTime);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}

