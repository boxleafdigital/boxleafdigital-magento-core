<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Api;

use Magento\Framework\Api\SearchCriteriaInterface;


interface RestLogRepositoryInterface
{

    /**
     * Save RestLog
     * @param \BoxLeafDigital\Core\Api\Data\RestLogInterface $restLog
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \BoxLeafDigital\Core\Api\Data\RestLogInterface $restLog
    );

    /**
     * Retrieve RestLog
     * @param string $restlogId
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($restlogId);

    /**
     * Retrieve RestLog matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \BoxLeafDigital\Core\Api\Data\RestLogSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete RestLog
     * @param \BoxLeafDigital\Core\Api\Data\RestLogInterface $restLog
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \BoxLeafDigital\Core\Api\Data\RestLogInterface $restLog
    );

    /**
     * Delete RestLog by ID
     * @param string $restlogId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($restlogId);
}

