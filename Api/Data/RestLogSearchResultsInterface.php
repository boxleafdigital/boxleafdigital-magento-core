<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Api\Data;


interface RestLogSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get RestLog list.
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface[]
     */
    public function getItems();

    /**
     * Set url_path list.
     * @param \BoxLeafDigital\Core\Api\Data\RestLogInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

