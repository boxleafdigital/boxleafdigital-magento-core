<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Api\Data;


interface RestLogInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const RESTLOG_ID = 'restlog_id';
    const PROCESS_TIME = 'process_time';
    const CREATED_AT = 'created_at';
    const REQUEST_PARAMS = 'request_params';
    const RESPONSE = 'response';
    const HEADERS = 'headers';
    const URL_PATH = 'url_path';
    const UPDATED_AT = 'updated_at';
    const RESPONSE_MESSAGE = 'response_message';

    /**
     * Get restlog_id
     * @return string|null
     */
    public function getRestlogId();

    /**
     * Set restlog_id
     * @param string $restlogId
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setRestlogId($restlogId);

    /**
     * Get url_path
     * @return string|null
     */
    public function getUrlPath();

    /**
     * Set url_path
     * @param string $urlPath
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setUrlPath($urlPath);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\Core\Api\Data\RestLogExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\Core\Api\Data\RestLogExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\Core\Api\Data\RestLogExtensionInterface $extensionAttributes
    );

    /**
     * Get request_params
     * @return string|null
     */
    public function getRequestParams();

    /**
     * Set request_params
     * @param string $requestParams
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setRequestParams($requestParams);

    /**
     * Get headers
     * @return string|null
     */
    public function getHeaders();

    /**
     * Set headers
     * @param string $headers
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setHeaders($headers);

    /**
     * Get response
     * @return string|null
     */
    public function getResponse();

    /**
     * Set response
     * @param string $response
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setResponse($response);

    /**
     * Get response_message
     * @return string|null
     */
    public function getResponseMessage();

    /**
     * Set response_message
     * @param string $responseMessage
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setResponseMessage($responseMessage);

    /**
     * Get process_time
     * @return string|null
     */
    public function getProcessTime();

    /**
     * Set process_time
     * @param string $processTime
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setProcessTime($processTime);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \BoxLeafDigital\Core\Api\Data\RestLogInterface
     */
    public function setUpdatedAt($updatedAt);
}

