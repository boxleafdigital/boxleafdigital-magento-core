<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Controller\Adminhtml\RestLog;


use Magento\Framework\Controller\Result\Forward;

/**
 * Class NewAction
 * @package BoxLeafDigital\Core\Controller\Adminhtml\RestLog
 * @author Daniel Coull <hello@boxleafdigital.com>
 */
class NewAction extends \BoxLeafDigital\Core\Controller\Adminhtml\RestLog
{

    protected $resultForwardFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * New action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}

