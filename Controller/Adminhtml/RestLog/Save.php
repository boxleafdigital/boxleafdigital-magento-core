<?php declare(strict_types=1);

namespace BoxLeafDigital\Core\Controller\Adminhtml\RestLog;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 * @package BoxLeafDigital\Core\Controller\Adminhtml\RestLog
 * @author Daniel Coull <hello@boxleafdigital.com>
 */
class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('restlog_id');

            $model = $this->_objectManager->create(\BoxLeafDigital\Core\Model\RestLog::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Request Log no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Request Log.'));
                $this->dataPersistor->clear('boxleafdigital_core_restlog');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['restlog_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Request Log.'));
            }

            $this->dataPersistor->set('boxleafdigital_core_restlog', $data);
            return $resultRedirect->setPath('*/*/edit', ['restlog_id' => $this->getRequest()->getParam('restlog_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
