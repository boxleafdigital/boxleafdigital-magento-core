<?php declare(strict_types=1);


namespace BoxLeafDigital\Core\Controller\Adminhtml\RestLog;

use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Delete
 * @package BoxLeafDigital\Core\Controller\Adminhtml\RestLog
 * @author Daniel Coull <hello@boxleafdigital.com>
 */
class Delete extends \BoxLeafDigital\Core\Controller\Adminhtml\RestLog
{

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('restlog_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\BoxLeafDigital\Core\Model\RestLog::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Request Log.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['restlog_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Request Log to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

