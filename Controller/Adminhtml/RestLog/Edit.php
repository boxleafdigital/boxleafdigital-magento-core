<?php declare(strict_types=1);

namespace BoxLeafDigital\Core\Controller\Adminhtml\RestLog;

use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Edit
 * @package BoxLeafDigital\Core\Controller\Adminhtml\RestLog
 * @author Daniel Coull <hello@boxleafdigital.com>
 */
class Edit extends \BoxLeafDigital\Core\Controller\Adminhtml\RestLog
{
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('restlog_id');
        $model = $this->_objectManager->create(\BoxLeafDigital\Core\Model\RestLog::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Request Log no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('boxleafdigital_core_restlog', $model);

        // 3. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Request Log') : __('New Request Log'),
            $id ? __('Edit Request Log') : __('New Request Log')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Request Logs'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Request Log %1', $model->getId()) : __('New Request Log'));
        return $resultPage;
    }
}
